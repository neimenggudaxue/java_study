package second;

import com.alibaba.fastjson.JSONObject;
import org.testng.annotations.Test;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午5:04 2018/12/28
 */
public class JsonTest {

    @Test
    public void test(){
        String str="{\n" +
                "    \"batNo\":\"12\",\n" +
                "    \"brNo\":\"qudian\",\n" +
                "    \"dataCnt\":1,\n" +
                "    \"dataCnt1\":1,\n" +
                "    \"dataCnt2\":1,\n" +
                "    \"list\":[{\"1\":1,\"2\":2},{\"3\":4}]\n" +
                "}";


        JSONObject temp=JSONObject.parseObject(str);


        System.out.println(temp.getString("list"));

    }
}
