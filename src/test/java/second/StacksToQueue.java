package second;

import org.testng.annotations.Test;

import java.util.Objects;
import java.util.Stack;

/**
 * @Description: 接口：
 * 用栈实现队列
 * @Author: qufang
 * @Date: Created in 下午3:06 2018/12/28
 */
public class StacksToQueue {
    Stack<Object> stack1=new Stack();
    Stack<Object> stack2=new Stack();
    public void addToTail(Object o){
        stack1.push(o);
    }

    public Object popFromHead(){
        if(stack1.size()+stack2.size()==0){
            System.out.println("队列已空，不能出队");
            return -1;
        }else {
            if (stack2.empty()){
                while (!stack1.isEmpty())
                    stack2.push(stack1.pop());
            }
            if (!stack2.empty()){
                return stack2.pop();
            }
        }
        return "";
    }

    @Test
    public void test(){
        StacksToQueue temp=new StacksToQueue();
        temp.addToTail(1);
        temp.addToTail(2);
        temp.addToTail(3);
        temp.addToTail(4);
        System.out.println(temp.popFromHead());
        temp.addToTail(5);
        System.out.println(temp.popFromHead());
        temp.addToTail(6);
        System.out.println(temp.popFromHead());
        System.out.println(temp.popFromHead());
        System.out.println(temp.popFromHead());
        System.out.println(temp.popFromHead());
        System.out.println(temp.popFromHead());
    }
}
