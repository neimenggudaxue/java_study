package second;

import org.testng.annotations.Test;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 队列实现栈
 * @Author: qufang
 * @Date: Created in 下午3:56 2018/12/28
 */
public class QueueToStack {
    Queue<Object> queue1=new LinkedList<Object>();
    Queue<Object> queue2=new LinkedList<Object>();

    public void push(Object o){
        queue1.offer(o);
    }

    public Object pop(){
        if (!queue1.isEmpty()){
            while (queue1.size()>1)
                queue2.offer(queue1.poll());
            Object code=queue1.poll();
            while (!queue2.isEmpty())
                queue1.offer(queue2.poll());
            return code;
        }else if(queue1.size()+queue2.size()==0){
            System.out.println("栈为空无元素可出！");
            return -1;
        }
        return -1;
    }

    @Test
    public void test(){
        QueueToStack temp=new QueueToStack();
        temp.push(1);
        temp.push(2);
        temp.push(3);
        System.out.println(temp.pop());
        temp.push(5);
        temp.push(6);
        System.out.println(temp.pop());
        System.out.println(temp.pop());
        System.out.println(temp.pop());
        temp.push(7);
        temp.push(8);
        System.out.println(temp.pop());
        System.out.println(temp.pop());
    }

    @Test
    public void test1(){
        QueueToStack temp=new QueueToStack();
        temp.push(1);
        System.out.println(temp.pop());
        System.out.println(temp.pop());
        System.out.println(temp.pop());
        System.out.println(temp.pop());
        System.out.println(temp.pop());
    }

}
