package second;

import org.testng.annotations.Test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description: 接口：
 * 比较版本号，前者大则返回正数否则返回负数
 * @Author: qufang
 * @Date: Created in 下午3:39 2018/12/27
 */
public class CompareVersion {

    public int getResult(String version1,String version2){
        int diff=0;
        int i=0,j=0;
        String array1[]=version1.split("\\.");
        String array2[]=version2.split("\\.");
        int minLen=Math.min(array1.length,array2.length);
        int m=0;
        while (m<minLen
                && (diff=array1[m].length()-array2[m].length())==0
                && (diff=array1[m].compareTo(array2[m]))==0
        ){
            m++;
        }
        diff=(diff!=0)?diff:array1.length-array2.length;
        return diff;
    }

    @Test
    public void test(){
        String version1="2.2.3q.4";
        String version2="13.2e.3b";
        int result=getResult(version1,version2);
        System.out.println(result);
    }

    // compareTo 方法使用
    @Test
    public void test1(){
        String version1="3q";
        String version2="3p";
        System.out.println(version1.compareTo(version2));
    }

    //split方法使用
    @Test
    public void test2(){
        String[] list="wo?men?kk".split("\\?");
        int m=0;
        while (m<list.length){
            System.out.print(list[m++]+" ");
        }
    }

    //split方法使用
    @Test
    public void test3(){
        String[] list="wo.men?kk.ll／oo.pp*henan".split("\\?|\\.|／|\\*");
        int m=0;
        while (m<list.length){
            System.out.print(list[m++]+" ");
        }
    }

    //输出一个电话号码
    @Test
    public void test4() throws Exception{
        String path=System.getProperty("user.dir");

        InputStream inputStream=new FileInputStream(path+"/src/test/java/first/file3.txt");
        BufferedInputStream bufferedInputstream=new BufferedInputStream(inputStream);
        byte[] bytes=new byte[2048];
        int n=bufferedInputstream.read(bytes,0,bytes.length);
        int i=0;
        String t="";
        while (i<bytes.length){
            t+=bytes[i++];
        }
       // System.out.println(t);
        String reg="13\\d9";
        Boolean result=Pattern.matches(reg,"13772311033");
        System.out.println(result);
        /*String reg="13[0-9]{9}";
        Pattern pattern=Pattern.compile(reg);
        Matcher matcher=pattern.matcher(bytes.toString());
        System.out.println(matcher.find());*/
    }
}
