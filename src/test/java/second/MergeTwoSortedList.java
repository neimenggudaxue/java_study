package second;

import org.testng.annotations.Test;
import sun.swing.BakedArrayList;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 上午11:38 2018/12/27
 */
public class MergeTwoSortedList {

    public int[] getResults(int[] a,int[] b){
        if(a.length==0)
            return b;
        if (b.length==0)
            return a;

        int[] c=new int[a.length+b.length];
        for (int i=0,j=0,k=0;i<a.length&j<b.length;){
            if (a[i]<=b[j]){
                c[k++]=a[i++];
            }
            else if (a[i]>b[j]){
                c[k++]=b[j++];
            }
            if(i==a.length&&j!=b.length){
                while (j<b.length)
                    c[k++]=b[j++];
                break;
            }else if(j==b.length&&i!=a.length){
                while (i<a.length)
                    c[k++]=a[i++];
                break;
            }
        }
        return c;
    }

    @Test
    public void test(){
        int[] a={2,2,2};
        int[] b={1,2,3,4};
        int []c=new int[a.length+b.length];
        c=getResults(a,b);
        int m=0;
        while (m<c.length)
            System.out.print(" "+c[m++]);
    }
}
