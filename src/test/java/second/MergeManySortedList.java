package second;

import org.testng.annotations.Test;

import java.util.Arrays;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午11:31 2019/1/3
 */
public class MergeManySortedList {
    @Test
    public int[] MergeArrays(int[][] array) {
        int N = array.length, L;
        if (N == 0)
            return new int[0];
        else {
            L = array[0].length;
            for (int i = 1; i < N; i++)
                if (L != array[i].length)
                    return new int[0];
        }
        int[] result = new int[N * L];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < L; j++)
                result[i * L + j] = array[i][j];
        Arrays.sort(result);
        return result;
    }
}
