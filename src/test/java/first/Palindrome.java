package first;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 判断是否为回文数练习
 * 接口类型：GET
 * @Author: qufang
 * @Date: Created in 下午6:27 2018/11/29
 */
public class Palindrome {
    public boolean isPalindrome(int x) {
        if (x<0)
            return false;
        List<Integer> bitList=new ArrayList();
        int count=0;
        int temp=10;
        while (x!=0){
            bitList.add(count,x%temp);
            x=(x-(x%temp))/temp;
            count++;
        }

        boolean result=true;
        if(count%2==0){
            for (int i=0;i<=(count-1)/2;i++){
                if (bitList.get(i)!=bitList.get(count-(i+1))) {
                    result = false;
                    break;
                }
            }
        }

        if (count%2!=0){
            for (int i=0;i<=(count-1)/2-1;i++){
                if (bitList.get(i)!=bitList.get(count-(i+1))) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    @Test
    public void test1(){
        boolean result=isPalindrome(123);
        System.out.println(result);
    }
    @Test
    public void test2(){
        boolean result=isPalindrome(121);
        System.out.println(result);
    }
    @Test
    public void test3(){
        boolean result=isPalindrome(1221);
        System.out.println(result);
    }
    @Test
    public void test4(){
        boolean result=isPalindrome(-1221);
        System.out.println(result);
    }

    @Test
    public void test5(){
        boolean result=isPalindrome(-12321);
        System.out.println(result);
    }
}
