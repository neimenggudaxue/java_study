package first;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Queue;
import java.util.Stack;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午11:04 2018/7/13
 */
public class TryCatch {

    @Test
    public void test1(){
        int[] arrInt={23,45,78};
        for (int i=0;i<4;i++)
            System.out.println(arrInt[i]);
    }

    @Test
    public void test(){
        /*int[] arrInt={23,45,78};
        try {
            for (int i=0;i<4;i++)
                System.out.println(arrInt[i]);

        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("越界啦");
            e.printStackTrace();
        }*/
        int [][]t=new int[3][4];
        System.out.println(t.length);
    }


}
