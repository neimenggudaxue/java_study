package first.threadPack;

import java.util.LinkedList;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午10:09 2018/12/26
 */
public class MyConsumer implements Runnable {

    @Override
    public void run(){
        synchronized(StockList.list){
            //System.out.println("消费者"+StockList.list.size());
            while (StockList.list.size()==0){
                try {
                    System.out.println("仓库已空");
                    StockList.list.wait();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            System.out.println("消费者消费成功！");
            StockList.list.remove();
            //System.out.println("222"+StockList.list.size());
            StockList.list.notifyAll();
        }
    }
}
