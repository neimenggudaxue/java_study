package first.threadPack;

import java.util.LinkedList;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午9:51 2018/12/26
 */
public class MyProducer implements Runnable{
    @Override
    public void run(){
        synchronized( StockList.list){
            //System.out.println("生产者"+StockList.list.size());
            while (StockList.list.size()==StockList.Max_stocks){
                try {
                    System.out.println("仓库已满！");
                    StockList.list.wait();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            StockList.list.add(new Object());
            System.out.println("生产者生产成功！");
           // System.out.println("111"+StockList.list.size());
            StockList.list.notifyAll();
        }
    }
}
