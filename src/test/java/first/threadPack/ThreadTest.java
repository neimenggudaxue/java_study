package first.threadPack;

import org.testng.annotations.Test;

import java.util.LinkedList;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午9:47 2018/12/26
 */
public class ThreadTest {

    @Test
    public void test(){
        MyProducer m1=new MyProducer();
        MyConsumer m2=new MyConsumer();
        for (int i=0;i<45;i++){
            Thread t1=new Thread(m1);
            t1.start();
        }

        for (int j=0;j<20;j++){
            Thread t2=new Thread(m2);
            t2.start();
        }
    }
}
