package first;

import org.testng.annotations.Test;

import java.io.*;

/**
 * @Author: qufang
 * @Date: Created in 上午11:23 2018/12/24
 */
public class JavaIO {

    public String pic_path = System.getProperty("user.dir") + "/src/test/java/first/pic";
    public String file_path = System.getProperty("user.dir") + "/src/test/java/first/files";


    @Test
    //InputStream、OutputStream（字节流）
    public void streamTest() throws IOException {
        File file1 = new File(pic_path + "/1.jpeg");
        if (!file1.exists()) {
            file1.createNewFile();
        }
        InputStream inputStream = new FileInputStream(file1);
        File file2 = new File(pic_path + "/1_1.jpeg");
        if (file2.exists()) {
            file2.delete();
        }
        file2.createNewFile();
        OutputStream outputStream = new FileOutputStream(file2);
        byte[] bytes = new byte[1024];  //一次性取多少字节
        int n = -1;
        while ((n = inputStream.read(bytes, 0, bytes.length)) != -1) {
            outputStream.write(bytes, 0, n);
        }
        inputStream.close();
        outputStream.close();
    }


    @Test
    //缓存字节流 BufferedInputstream
    public void bufferStreamtest() throws Exception {
        InputStream inputStream = new FileInputStream(file_path + "/file1.txt");
        OutputStream outputStream = new FileOutputStream(file_path + "/file1_1.txt");
        BufferedInputStream in = new BufferedInputStream(inputStream);
        BufferedOutputStream out = new BufferedOutputStream(outputStream);
        int n = -1;
        byte[] cache = new byte[1024];
        n = in.read(cache, 0, cache.length);
        while (n != -1) {
            out.write(cache, 0, n);
            n = in.read(cache, 0, cache.length);
        }
        out.flush();
        in.close();
        out.close();
    }

    @Test
    //缓冲字符流 操作文本文件
    public void bufferReaderTest() throws Exception {
        try {
            BufferedReader in = new BufferedReader(new FileReader(file_path + "/file1.txt"));
            BufferedWriter out = new BufferedWriter(new FileWriter(file_path + "/file1_2.txt"));
            String str = null;
            while ((str = in.readLine()) != null) {
                // System.out.println(str);
                out.write(str);
                out.newLine();
            }
            out.flush();
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //FileReader 操作文本文件
    @Test
    public void fileReaderTest() throws Exception {
        try {
            FileReader fileReader = new FileReader(new File(file_path + "/file1.txt"));
            FileWriter fileWriter = new FileWriter(new File(file_path + "/file1_3.txt"));
            char bytes[] = new char[1024];
            int n = 0;
            while ((n = fileReader.read(bytes, 0, bytes.length)) != -1)
                //fileWriter.write(bytes);
                fileWriter.write(bytes, 0, n);
            fileReader.close();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //InputStreamReader 操作文本文件
    @Test
    public void streamReader() throws Exception {
        try {
            InputStreamReader in = new InputStreamReader(new FileInputStream(file_path + "/file1.txt"));
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file_path + "/file1_4.txt"));
            byte bytes[] = new byte[1024];
            int n = 0;
            while ((n = in.read()) != -1)
                out.write(n);
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //随机读写文件操作

    @Test
    public void randomAccessTest() throws Exception{
        try {
            RandomAccessFile randomAccessFile=new RandomAccessFile(new File(file_path + "/file1.txt"),"rw");
            RandomAccessFile out=new RandomAccessFile(new File(file_path + "/file1_5.txt"),"rw");
            Long len=randomAccessFile.length();
            System.out.println("文件file1的长度为："+len);
            int n=-1;
            byte bytes[]=new byte[1024];
            randomAccessFile.seek(1024);
            while ((n=randomAccessFile.read(bytes,0,bytes.length))!=-1){
                out.write(bytes,0,n);
            }
            System.out.println("文件file1_5的长度为："+out.length());
            randomAccessFile.close();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

