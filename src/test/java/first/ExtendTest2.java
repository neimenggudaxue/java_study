package first;

import org.testng.annotations.Test;

class Animal{
    public int num=55;
    public String text="hello";

    public void say(){
        System.out.println("animal saying-----");
    }
}
class Wolf extends Animal{
    public int num=33;
    public void say(){
        System.out.println("wolf saying =====");
    }
}

public class ExtendTest2 {

    @Test
    public void test(){
        Animal animal=new Animal();
        System.out.println(animal.num);
        System.out.println(animal.text);
        animal.say();
        Animal aa=new Wolf();
        aa.say();
    }

    @Test
    public void test2(){
        Wolf wolf=new Wolf();
        System.out.println(wolf.num);
        System.out.println(wolf.text);
        wolf.say();

    }
}
