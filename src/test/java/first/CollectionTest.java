package first;

import org.testng.annotations.Test;

import java.util.*;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午10:45 2018/12/24
 */
public class CollectionTest {

    @Test
    //队列
    public void test1(){
        Queue queue=new LinkedList(); //LinkedList(基于链的线性表)，当然也可以使用ArrayList(基于数组的线性表)
        //不推荐使用add(),remove(),因为方法在失败的时候会跑出异常
        /*queue.add("a");
        queue.add("b");
        queue.add("c");
        queue.add("d");*/
        queue.offer("a");
        queue.offer("b");
        queue.offer("c");
        queue.offer("d");
        System.out.println(queue.size());
        System.out.println(queue);
        Iterator iterator=queue.iterator();
        for (Object o:queue) {
            System.out.println(iterator.next());
        }

        // peek()返回当前队列中最早进入的元素
        System.out.println(queue.peek());
        System.out.println(queue);

        // poll()返回当前队列中最早进入的元素，并在队列中删除
        System.out.println(queue.poll());
        System.out.println(queue);

        /*System.out.println(queue.remove());
        System.out.println(queue);*/

        // element()返回当前队列中最早进入的元素
        System.out.println(queue.element());
        System.out.println(queue);

        System.out.println(queue.isEmpty());

    }

    @Test
    // 栈
    public void test2(){
        Stack stack=new Stack();
        stack.push(356);
        stack.push("e");
        stack.push('l');
        stack.push(1);
        stack.push(1.02);
        System.out.println("stack__"+stack);
        System.out.println("stack.size()__"+stack.size());
        System.out.println("empty()__"+stack.empty());
        System.out.println("获取指定位置的元素__"+stack.get(1));

        System.out.println("返回栈中最后入栈的元素__"+stack.peek());
        System.out.println("stack__"+stack);
        System.out.println("返回并删除栈中最后入栈的元素__"+stack.pop());
        System.out.println("stack__"+stack);
        //使用迭代器遍历栈
        Iterator iterator=stack.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
            iterator.remove();
        }
        System.out.println("清空中后的栈"+stack);
    }

    @Test
    //HashTable 的实现实现了Map接口,存储的是健值对。
    public void test3(){
        Hashtable hashtable=new Hashtable();
        // 哈希表中加入元素
        hashtable.put("who","me");
        hashtable.put("where","厦门");
        hashtable.put("how","wait");
        System.out.println("哈希表："+hashtable);
        System.out.println("哈希表的长度："+hashtable.size());
        System.out.println("根据指定的key获取value:"+hashtable.get("who"));
        System.out.println("判断指定值是否在表中："+hashtable.contains("北京"));
        System.out.println("判断指定值是否在表中："+hashtable.contains("厦门"));
        System.out.println("判断指定key是否存在："+hashtable.containsKey("cuiic"));
        System.out.println("判断指定key是否存在："+hashtable.containsKey("who"));
        //清空哈希表的健
        hashtable.clear();
        System.out.println("哈希表："+hashtable);
        System.out.println("哈希表的长度："+hashtable.size());
    }

    @Test
    // HashMap 实现了Map接口，存储的是健值对
    public void test4(){
        Map hashMap=new HashMap();
        System.out.println("hashMap的长度："+hashMap.size());
        System.out.println("判断hashMap是否为空:"+hashMap.isEmpty());
        hashMap.put("name","flower");
        hashMap.put("age",13);
        hashMap.put("school","通辽市");
        System.out.println("hashMap:"+hashMap);
        System.out.println("hashMap的长度："+hashMap.size());
        System.out.println("判断hashMap是否为空:"+hashMap.isEmpty());
        hashMap.remove("name");
        System.out.println("移除name这个key");
        System.out.println("hashMap的长度："+hashMap.size());
        System.out.println("判断hashMap是否为空:"+hashMap.isEmpty());
        System.out.println("hashMap:"+hashMap);
        hashMap.replace("school","lili");
        System.out.println("hashMap的长度："+hashMap.size());
        System.out.println("判断hashMap是否为空:"+hashMap.isEmpty());
        System.out.println("hashMap:"+hashMap);

    }

    @Test
    // hashSet 实现了set接口，有重复元素插入则只保留1个值
    public void test5(){
        Set hashSet=new HashSet();
        hashSet.add("a");
        hashSet.add("a");
        hashSet.add("b");
        hashSet.add("c");
        hashSet.add("b");
        System.out.println(hashSet.size());
        System.out.println(hashSet);
    }
}
