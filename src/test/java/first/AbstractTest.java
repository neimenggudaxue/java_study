package first;

import org.testng.annotations.Test;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午7:14 2018/7/14
 */

abstract class Qshapes{
    public void call(){
        System.out.println("this is calling method ");
    }
    public abstract void sleep();
}

class Sheep extends Qshapes{
   public void sleep(){
       System.out.println("this is Sheep sleeping");
    }
}

public class AbstractTest {

    @Test
    public void test(){
        Sheep sheep=new Sheep();
        sheep.call();
        sheep.sleep();
    }
}
