package first;

import org.testng.annotations.Test;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午9:09 2018/7/13
 */

class Parent{
    String name;
    int age=2;

    public void setName1() {
        this.name = "parent";
    }

    public String getName() {
        return name;
    }
}

class Son extends Parent{

    int age=3;

    public void getParentAge(){
        System.out.println(super.age);
    }

    public void checkName(){
        System.out.println(name);
        super.setName1();
        System.out.println(name);
        System.out.println("----------------");
        this.name="son";
        System.out.println(super.getName());
    }

}


public class ExtendTest {
    @Test
    public void test(){
        Son son=new Son();
        son.checkName();
    }

    @Test
    public void test2(){
        Son son=new Son();
        System.out.println("sou.age="+son.age);
        System.out.print("parent.age=");
        son.getParentAge();
    }


}
