package first;

import org.testng.annotations.Test;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午7:03 2018/7/14
 */
interface Shapes{
    void draw();
    void erase();

}

class Square implements Shapes{
    public void draw(){
        System.out.println("this is Square draw");
    }
    public void erase(){
        System.out.println("this is Square erase");
    }

}

public class InterfaceTest {

    @Test
    public void test1(){
        Square square=new Square();
        square.draw();
        square.erase();
    }
}
