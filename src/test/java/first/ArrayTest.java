package first;

import org.testng.annotations.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午8:36 2018/7/11
 */
public class ArrayTest {
    @Test
    public void initialArray(){

        // 静态初始化数组方式 一
        String[] arrayBooks=new String[]{
                "sanguo",
                "shuihu",
                "lieren"
        };

        // 静态初始化数组方式 二
        String[] names={
                "sun wukong",
                "zhubajie"
        };

        // 动态初始化数组方式 一
        String[] strArray=new String[5];

        System.out.println(arrayBooks.length);
        System.out.println(names.length);
        System.out.println(strArray.length);
    }

    @Test
    public void test5(){
        String str="hello momo";
        if (str.contains("hello")){
            System.out.println(str);
        }
        String str1="2018-12-13 qufang@qudian.com  qq  cost[5000000]ms";
        //int a = str1.lastIndexOf("cost[");
        //int b = str1.lastIndexOf("]ms");
        //System.out.print(str1.substring(a+5, b));
        Pattern p = Pattern.compile("^*cost\\[(\\d*)\\]ms*$");//获取正则表达式中的分组，每一组小括号为一组
        Matcher m = p.matcher(str1);//进行匹配
        if (m.find()) {//判断正则表达式是否匹配到
            System.out.print(m.group(1));
        }

    }
}
