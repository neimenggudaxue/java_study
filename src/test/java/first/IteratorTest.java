package first;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 上午9:39 2018/12/19
 */
public class IteratorTest {
    public Iterator<String []> howCreate(){
        List<String[]> list=new ArrayList<>();
        String temp1[]={"1","2","3"};
        temp1[0]="a0";
        temp1[1]="a1";
        temp1[2]="a2";

        String temp2[]={"1","2"};
        temp2[0]="b0";
        temp2[1]="b1";
        //temp1[2]="b2";

        list.add(temp1);
        list.add(temp2);

        Iterator<String[]> iterator=list.iterator();
        while (iterator.hasNext()){
            String tt[]={""};
            tt=iterator.next();
            for (int i=0;i<tt.length;i++){
                System.out.println(tt[i]);
            }
        }
        return iterator;
    }


    @Test
    public void test1(){
        howCreate();
    }
}
