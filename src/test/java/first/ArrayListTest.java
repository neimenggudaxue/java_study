package first;

import org.testng.annotations.Test;

import java.util.*;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午10:04 2018/11/28
 */
public class ArrayListTest {

    public int reverse(int x) {
        int result = 0;
        int count = 0;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        List<Integer> bitList = new ArrayList();
        int temp = 10;
        while (x != 0) {
            bitList.add(count, x % temp);
            x = (x - (x % temp)) / temp;
            count++;
        }
        int bitNum = 1;
        for (int i = bitList.size() - 1; i >= 0; i--) {
            result += bitList.get(i) * bitNum;
            bitNum *= 10;
        }
        return result;
    }

    @Test
    public void test1() {
        System.out.println(reverse(159));
    }

    @Test
    public void test2() {
        System.out.println(reverse(-159));
    }

    @Test
    public void test3() {
        System.out.println(reverse(200));
    }

    @Test
    public void test4() {
        System.out.println(reverse(203));
    }

    @Test
    public void test5() {
        List books = new ArrayList();
        //向books集合中添加三个元素
        books.add(new String("轻量级Java EE企业应用实战"));
        books.add(new String("疯狂Java讲义"));
        books.add(new String("疯狂Android讲义"));
        System.out.println(books);

        //将新字符串对象插入在第二个位置
        books.add(1, new String("疯狂Ajax讲义"));
        for (int i = 0; i < books.size(); i++) {
            System.out.println(books.get(i));
        }

        //删除第三个元素
        books.remove(2);
        System.out.println(books);

        //判断指定元素在List集合中位置：输出1，表明位于第二位
        System.out.println(books.indexOf(new String("疯狂Ajax讲义")));  //①
        //将第二个元素替换成新的字符串对象
        books.set(1, new String("LittleHann"));
        System.out.println(books);

        //将books集合的第二个元素（包括）
        //到第三个元素（不包括）截取成子集合
        System.out.println(books.subList(1, 2));
    }

    @Test
    public void test6() {
        List<String> list1 = new ArrayList<>();
        list1.add("a");
        list1.add("b");
        //list.add(12);  因为定义list的时候是用List<String>来限定的，所以list里的元素只能是string,如果不限定，是可以任何元素的

        List list2 = new ArrayList();
        list2.add("dd");
        list2.add(12);
        list2.add(Integer.parseInt("45"));
        System.out.println(list1);
        System.out.println(list2);

    }

    @Test
    public void test7() {
        ArrayList list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        System.out.println(list);
        list.remove(1);
        System.out.println(list);
    }

    @Test
    public void test8() {
        LinkedList list = new LinkedList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(1, 5);
        System.out.println(list);
        list.remove();
        System.out.println(list);

        LinkedList list2 = new LinkedList();
        list2.add(12);
        list2.add(11);
        list2.add(10);
        list.addAll(list2);
        System.out.println(list);
    }

    @Test
    // LinkedList实现栈,插入的时候相当于是前插发，永远插在最前边，
    public void test9() {
        LinkedList list = new LinkedList();
        list.addFirst(1);
        list.addFirst(2);
        list.addFirst(3);
        System.out.println(list);
        while (!list.isEmpty()) {
            System.out.println(list.removeFirst());
        }
    }

    //Linklist实现队列，
    @Test
    public void test10() {
        LinkedList list = new LinkedList();
        list.addLast(1);
        list.addLast(2);
        list.addLast(3);
        list.offerLast(4);
        list.offerLast(5);
        list.offerLast(6);
        System.out.println(list);
        while (!list.isEmpty()) {
            System.out.println(list.pollFirst());
        }
    }
}
