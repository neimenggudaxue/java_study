package first;

import org.testng.annotations.Test;

/**
 * @Description: 接口：
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 下午7:49 2018/7/13
 */

class Employee{
    public String name;

    public String getName() {
        return name;
    }

    public void say(){
        System.out.println("Employee is saying!");
    }

    public void cry(){
        System.out.println("Employee is crying !");
    }

    public void initialName(){
        name="qq";
    }
}

class Manager extends Employee{
    public void say(){
        System.out.println("this is Manager saying!");
    }

    public void cry(){
        super.cry();
    }

    public void see(){
        System.out.println("Manager seeing!");
        System.out.println(name);
    }

    public void getNameManager(){
        System.out.println("我是子类"+name);
        super.initialName();
        System.out.println("父类初始化name='qq'后子类的name"+name);
        this.name="我是谁";
        System.out.println("子类初始化name='我是谁'后子类的name'"+name);
        System.out.println("子类初始化name='我是谁'后父类的name'"+super.name);
    }
}


public class TypeConversion {
    //子类向父类靠齐
    @Test
    public void test(){
        Employee temp;
        temp=new Manager();
        temp.cry();
    }

    //父类向子类看齐 X
    @Test
    public void test1(){
        Manager temp;
        temp=(Manager) new Employee();
        temp.cry();
    }

    @Test
    public void test2(){
        Manager temp=new Manager();
        temp.getNameManager();
        Employee e=new Employee();
        System.out.println(e.getName());
    }
}
