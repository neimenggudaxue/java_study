package leetCode;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 13.罗马数字转整数
 * 接口类型：GET
 * 参数：
 * 例子： GET
 * @Author: qufang
 * @Date: Created in 上午11:33 2018/11/30
 */
public class RomanToInt_13 {
    public int romanToInt(String s) {
        Map relation = new HashMap();
        relation.put("I", 1);
        relation.put("V", 5);
        relation.put("X", 10);
        relation.put("L", 50);
        relation.put("C", 100);
        relation.put("D", 500);
        relation.put("M", 1000);

        int result = 0;

        for (int i = 0; i < s.length(); ) {
            String x_key = String.valueOf(s.charAt(i));
            int x_value = (Integer) relation.get(x_key);
            String y_key = "";
            int y_value = 0;

            // 1.考虑只有一个罗马字符
            // 2.考虑当前罗马字符为字符串中最后一个字符
            if (i == s.length() - 1) {
                result += x_value;
                i++;
            }
            //当前罗马字符不是最后一个罗马字符
            if (i < s.length() - 1) {
                int temp = 0;
                y_key = String.valueOf(s.charAt(i + 1));
                y_value = (Integer) relation.get(y_key);

                if(x_key.equals("I")&&y_key.equals("V")){
                    System.out.println("ppppp");
                    temp=4;
                    i+=2;
                }

                else if(x_key.equals("I")&&y_key.equals("X")){
                    temp=9;
                    i+=2;
                }

                else if(x_key.equals("X")&&y_key.equals("L")){
                    temp=40;
                    i+=2;
                }

                else if(x_key.equals("X")&&y_key.equals("C")){
                    temp=90;
                    i+=2;
                }

                else if(x_key.equals("C")&&y_key.equals("D")){
                    temp=400;
                    i+=2;
                }

                else if(x_key.equals("C")&&y_key.equals("M")){
                    temp=900;
                    i+=2;
                }

                else {
                    temp = x_value;
                    i++;
                }
                result += temp;
            }
        }
        System.out.println(result);
        return result;
    }
    @Test
    public void test1() {
        romanToInt("I");
    }

    @Test
    public void test2() {
        romanToInt("V");
    }

    @Test
    public void test3() {
        romanToInt("X");
    }

    @Test
    public void test4() {
        romanToInt("III");
    }

    @Test
    public void test5() {
        romanToInt("IL");
    }

    @Test
    public void test20() {
        romanToInt("IV");
    }

}
