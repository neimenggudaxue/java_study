package leetCode;

import org.testng.annotations.Test;

/**
 * @Description: 接口：搜索插入位置
 * @Author: qufang
 * @Date: Created in 下午8:52 2018/12/5
 */
public class SearchInsert_35 {
    public int searchInsert(int[] nums, int target) {
        if(target<=nums[0])
            return 0;
        else if(target>nums[nums.length-1])
            return nums.length;
        else {
            int mid=nums.length/2;
            if(target==nums[mid])
                return mid;
            else if(target<nums[mid]){
                for (int i=1;i<=mid;i++){
                    if (target==nums[i])
                        return i;
                    else if(target>nums[i]&& target<nums[i+1])
                        return i+1;
                    else if(target<nums[i] && target>nums[i-1])
                        return i;
                }// 结束条件 i=1;
            }else if(target>nums[mid]){
                for (int i=mid;i<nums.length-1;i++){
                    if (target==nums[i])
                        return i;
                    else if(target>nums[i]&& target<nums[i+1])
                        return i+1;
                    else if(target<nums[i] && target>nums[i-1])
                        return i;
                }//结束条件 i=nums.length-1
                if (target==nums[nums.length-1])
                    return nums.length-1;
                else if(target>nums[nums.length-1])
                    return nums.length;
                else return nums.length-2;
            }
        }
        return 0;
    }

    @Test
    public void test1(){
        int[] nums={1,5,6,8};
        int target=5;
        System.out.println(searchInsert(nums,target));
    }

    @Test
    public void test2(){
        int[] nums={1};
        int target=1;
        System.out.println(searchInsert(nums,target));
    }

    @Test
    public void test3(){
        int[] nums={1,3,5,6};
        int target=2;
        System.out.println(searchInsert(nums,target));
    }

    @Test
    public void test4(){
        int[] nums={1,3,5};
        int target=5;
        System.out.println(searchInsert(nums,target));
    }
}
