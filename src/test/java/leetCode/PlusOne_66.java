package leetCode;

import org.testng.annotations.Test;

/**
 * @Description: 加一
 * @Author: qufang
 * @Date: Created in 下午7:58 2018/12/6
 */
public class PlusOne_66 {
    public int[] plusOne(int[] digits) {
        if (digits.length == 1 && digits[0] != 9) {
            digits[0] = digits[0] + 1;
            return digits;
        } else if (digits.length == 1 && digits[0] == 9) {
            int[] result = new int[digits.length + 1];
            result[0] = 1;
            result[1] = 0;
            return result;
        } else {
            int i = digits.length - 1;
            for (; i > 0; i--) {
                if (digits[i] != 9) {
                    digits[i] = digits[i] + 1;
                    return digits;
                } else if (digits[i] == 9 && digits[i - 1] != 9) {

                    digits[i - 1] = digits[i - 1] + 1;
                    for (int k = i; k < digits.length; k++) {
                        digits[k] = 0;
                    }
                    break;
                }
            }
            if (i == 0) { //所有元素都是9
                int[] result = new int[digits.length + 1];
                result[0] = 1;
                for (int k = 1; k < result.length; k++) {
                    result[k] = 0;
                }
                return result;
            }
        }
        return digits;
    }

    @Test
    public void test1() {
        int[] digits = {1, 2, 3};
        plusOne(digits);
    }

    @Test
    public void test2() {
        int[] digits = {9};
        plusOne(digits);
    }

    @Test
    public void test3() {
        int[] digits = {9, 9, 9};
        plusOne(digits);
    }
}
