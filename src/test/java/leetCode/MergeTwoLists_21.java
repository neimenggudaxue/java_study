package leetCode;

import org.testng.annotations.Test;


/**
 * @Description: 合并两个有序链表
 * @Author: qufang
 * @Date: Created in 下午3:03 2018/12/1
 */
public class MergeTwoLists_21 {

    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    /*public class LinkList{
        public ListNode head; //对第一个节点的引用
        public ListNode current; //对下一个节点的引用
        public void add(int data){
            if(head==null){//如果链表结构不存在
                head=new ListNode(data);
                current=head; //引用指向本节点在只有一个节点的情况下
            }else {
                current.next=new ListNode(data); //把本节点类中的成员变量next设为对下一个节点的引用
                current=current.next; //指向下一个节点
            }
        }
    }*/

    //遍历第二个链表往第一个链表中插入数据
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode return_list = new ListNode(0);

        if (l1 == null && l2 == null)
            return null;


        //return_list.next = null;
        ListNode tail = return_list;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                tail.next = l1;
                l1 = l1.next;
                tail = tail.next;
            } else {
                tail.next = l2;
                l2 = l2.next;
                tail = tail.next;
            }

        }
        if (l1 == null)
            tail.next = l2;
        else if (l2 == null)
            tail.next = l1;
        return return_list.next;
    }
}
