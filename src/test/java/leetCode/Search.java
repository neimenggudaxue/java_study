package leetCode;

import org.testng.annotations.Test;

import java.util.LinkedList;

/**二分查找
 * @Author: qufang
 * @Date: Created in 下午5:12 2018/12/9
 */
public class Search {
    public int search(int[] nums, int target) {
        int left=0,right=nums.length-1,mid=-1;
        while (left<=right){
            mid=(left+right)/2;
            if (nums[mid]<target){
                left=mid+1;
            }else if(nums[mid]>target){
                right=mid-1;
            }else if(nums[mid]==target){
                return mid;
            }
        }
        return -1;
    }

    @Test
    public void test1(){
        int[] nums={-1,0,3,5,9,12};
        int target=0;
        System.out.println(search(nums,target));
    }

    @Test
    public void test2(){
        int[] nums={2,5};
        int g=3;
        LinkedList temp=new LinkedList();
        temp.add(g);



        int target=0;
        System.out.println(search(nums,target));
    }
}
