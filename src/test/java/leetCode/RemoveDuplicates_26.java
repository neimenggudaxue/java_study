package leetCode;

import org.testng.annotations.Test;

/**
 * @Description: 删除排序数组中的重复项
 * @Author: qufang
 * @Date: Created in 上午10:07 2018/12/3
 */
public class RemoveDuplicates_26 {
    public int removeDuplicates(int[] nums) {
        int length=nums.length;
        if (length==0 || length==1){
            System.out.println(length);
            return length;
        }
        for (int i=1;i<length;i++){
            if(nums[i]==nums[i-1]){
                for (int k=i-1;k<length-1;k++){
                    nums[k]=nums[k+1];
                }
                length--;
                i--;
            }
        }
        System.out.println(length);
        return length;
    }


    @Test
    public void test0(){
        int[] temp={0,0,1,1,1,2,2,3,3,4};
        removeDuplicates(temp);
    }

    @Test
    public void test1(){
        int[] temp={1,1,2,2};
        removeDuplicates(temp);
    }
    @Test
    public void test2(){
        int[] temp={1,1,2,2,4};
        removeDuplicates(temp);
    }

    @Test
    public void test3(){
        int[] temp={1};
        removeDuplicates(temp);
    }
    @Test
    public void test4(){
        int[] temp={};
        removeDuplicates(temp);
    }

    @Test
    public void test5(){
        int[] temp={1,2,3,4,5};
        removeDuplicates(temp);
    }

}
