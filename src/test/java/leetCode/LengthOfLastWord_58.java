package leetCode;

import org.testng.annotations.Test;

/**
 * @Author: qufang
 * @Date: Created in 下午6:46 2018/12/6
 */
public class LengthOfLastWord_58 {
    public int lengthOfLastWord(String s) {
        int i=s.length()-1;
        if (i==-1 || s.trim().length()==0) {
            return 0;
        } else {
            int end = 0;
            for (; i >= 0; i--) {
                if (s.charAt(i) == 32 && end != 0) {
                    break;
                } else if (s.charAt(i) != 32 && end == 0) {
                    end = i;
                }
            }
            return end - i;
        }
    }

    @Test
    public void test1(){
        System.out.println(lengthOfLastWord("we "));
    }

    @Test
    public void test2(){
        System.out.println(lengthOfLastWord("qwe we"));
    }
    @Test
    public void test3(){
        System.out.println(lengthOfLastWord(""));
    }
    @Test
    public void test4(){
        System.out.println(lengthOfLastWord("Hello World"));
    }
    @Test
    public void test5(){
        System.out.println(lengthOfLastWord(" Hello World "));
    }
    @Test
    public void test6(){
        System.out.println(lengthOfLastWord(" "));
    }
}
