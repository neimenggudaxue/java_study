package leetCode;

import org.testng.annotations.Test;

/**
 * @Description: 移除元素
 * @Author: qufang
 * @Date: Created in 上午10:47 2018/12/4
 */
public class RemoveElement_27 {
    public int removeElement(int[] nums, int val) {
        int length=nums.length;
        if(length==1 && nums[length-1]==val)
            return --length;

        for (int i=length-1;i>=0;i--){
            if (nums[i]==val){
                if (i==length-1) //如果最后一个元素的值为目标值，则直接将数组长度-1
                    length--;
                else if (i!=length-1){
                    for (int k=i;k<length-1;k++){
                        nums[k]=nums[k+1];
                    }
                    length--;
                }
            }
        }
        System.out.println("length "+length);
        return length;
    }

    @Test
    public void test1(){
        int nums[]={1,2,3,3};
        removeElement(nums,3);
    }

    @Test
    public void test2(){
        int nums[]={1,3,4,5};
        removeElement(nums,3);
    }

    @Test
    public void test3(){
        int nums[]={1,3,4,5,7,8};
        removeElement(nums,3);
    }
    @Test
    public void test4(){
        int nums[]={1};
        removeElement(nums,1);
    }

    @Test
    public void test5(){
        int nums[]={};
        removeElement(nums,1);
    }
    @Test
    public void test6(){
        int nums[]={3,3};
        removeElement(nums,3);
    }
}
