package leetCode;

import org.testng.annotations.Test;

/** x 的平方根
 * @Author: qufang
 * @Date: Created in 下午3:23 2018/12/7
 */
public class MySqrt_69 {
    public int mySqrt(int x) {
        int left=1,right=(x+1)/2,mid=0;
        while (left<right){
            mid=(left+right)/2;
            int result=mid*mid;
            if(result>x){
                right=mid-1;
            }else if(result<x){
                left=mid+1;
            }
        }
        if(right*right>x)
            return right-1;
        else return right;
    }

    @Test
    public void test1(){
        int x=8;
        System.out.println(mySqrt(x));
    }
    @Test
    public void test2(){
        int x=9;
        System.out.println(mySqrt(x));
    }

    @Test
    public void test3(){
        int x=5;
        System.out.println(mySqrt(x));
    }
    @Test
    public void test4(){
        int x=1;
        System.out.println(mySqrt(x));
    }
}
