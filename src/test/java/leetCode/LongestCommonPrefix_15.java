package leetCode;
import org.testng.annotations.Test;
import sun.swing.BakedArrayList;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 最长公共前缀
 * 接口类型：https://leetcode-cn.com/problems/longest-common-prefix/
 * @Author: qufang
 * @Date: Created in 下午5:21 2018/11/30
 */
public class LongestCommonPrefix_15 {
    public String longestCommonPrefix(String[] strs) {
        String strResult="";
        if(strs.length==0)
            return "";
        int shortestWordindex = 0;
        for (int i = 0; i < strs.length - 1; i++) {
            if(strs[i].length()==0)
                return "";
            if (strs[shortestWordindex].length() > strs[i + 1].length())
                shortestWordindex = i + 1;
        }
        int shortestWordlength = strs[shortestWordindex].length() / 2;
        List<Character> result = new ArrayList();
        if(strs[shortestWordindex].length()==0 )
            return "";

        else if (strs[shortestWordindex].length() == 1) {//即包含长度为1的元素
            int count=0;
            for (int k = 0; k < strs.length - 1; k++) {
                if (strs[k].charAt(0) != strs[k + 1].charAt(0))
                    break;
                else
                    count++;
            }
            if (count==strs.length-1){
                //System.out.println("lllll"+strs[0].charAt(0));
                //System.out.println("pppp "+String.valueOf(strs[0].charAt(0)));
                return String.valueOf(strs[0].charAt(0));
            }else
                return "";
        } else {
            for (int j = 0; j <= shortestWordlength - 1; j++) {
                int count = 0;
                for (int k = 0; k < strs.length - 1; k++) {
                    if (strs[k].charAt(j) != strs[k + 1].charAt(j))
                        break;
                    else
                        count++;
                }
                if (count == strs.length - 1) { //即数组中所有该位置的字符都相等
                    result.add(strs[0].charAt(j));
                } else {
                    break;
                }
            }
            if (result.size() == shortestWordlength) { //即shortestWordlength下的所有元素都是相等的
                for (int m = shortestWordlength; m <= strs[shortestWordindex].length() - 1; m++) {
                    int count = 0;
                    for (int k = 0; k < strs.length - 1; k++) {
                        if (strs[k].charAt(m) != strs[k + 1].charAt(m))
                            break;
                        else
                            count++;
                    }
                    if (count == strs.length - 1) { //即数组中所有该位置的字符都相等
                        result.add(strs[0].charAt(m));
                    } else {
                        break;
                    }
                }
            }

            for (int l = 0; l < result.size(); l++) {
                strResult+=result.get(l);
            }
        }
        return strResult;
    }

    @Test
    public void test1() {
        String[] strs = {"qwetuir", "qwetr", "qwertt", "qwertty"};
        longestCommonPrefix(strs);
    }

    @Test
    public void test2() {
        String[] strs = {"ik", "f", "g", "h"};
        longestCommonPrefix(strs);
    }
    @Test
    public void test3() {
        String[] strs = {"ik", "f", "g", "[]"};
        longestCommonPrefix(strs);
    }
    @Test
    public void test4() {
        String[] strs ={};
        longestCommonPrefix(strs);
    }
    @Test
    public void test5() {
        String[] strs ={"aaa","aa","aaa"};
        longestCommonPrefix(strs);
    }
    @Test
    public void test6() {
        String[] strs ={"","cbc","c","ca"};
        longestCommonPrefix(strs);
    }
    @Test
    public void test7() {
        String[] strs ={"flower","flow","flight"};
        longestCommonPrefix(strs);
    }

    @Test
    public void test8() {
        String[] strs ={"abbb","a","accc","aa"};
        longestCommonPrefix(strs);
    }
}
