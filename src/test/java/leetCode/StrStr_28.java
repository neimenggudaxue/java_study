package leetCode;

import org.testng.annotations.Test;

/**
 * @Description: 实现strStr()
 * @Author: qufang
 * @Date: Created in 上午11:52 2018/12/4
 */
public class StrStr_28 {
    public int strStr(String haystack, String needle) {
        if(needle.length()==0)
            return 0;
        if(needle.length()>haystack.length())
            return -1;

        int i=0,j=0,start=0;
        for (;i<haystack.length();i++){
            if(haystack.length()-i<needle.length()) //下一轮即将开始的点到末尾的长度<needle.length，则证明前串中一定不包含后串
                break;
            j=0;
            if(haystack.charAt(i)==needle.charAt(j)){
                int m=i+1,k=j+1;
                j=k;
                if (m>=haystack.length())
                    break;
                for (;k<needle.length();){
                    if (haystack.charAt(m)==needle.charAt(k)){
                        k++;
                        m++;
                    }
                    else
                        break;
                }
                j=k;
            }
            if(j==needle.length())
                break;
        }
        if (j==needle.length()){
            start=i;
        }else
            start=-1;
        System.out.println("start "+start);
        return start;
    }

    @Test
    public void test1(){
        String haystack="acabd";
        String needle="ab";
        strStr(haystack,needle);
    }
    @Test
    public void test2(){
        String haystack="123";
        String needle="";
        strStr(haystack,needle);
    }

    @Test
    public void test3(){
        String haystack="ab";
        String needle="ab";
        strStr(haystack,needle);
    }

    @Test
    public void test4(){
        String haystack="";
        String needle="a";
        strStr(haystack,needle);
    }
    @Test
    public void test5(){
        String haystack="mississippi";
        String needle="issipi";
        strStr(haystack,needle);
    }
    @Test
    public void test6(){
        String haystack="a";
        String needle="a";
        strStr(haystack,needle);
    }
}

