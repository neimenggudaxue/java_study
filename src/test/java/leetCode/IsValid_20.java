package leetCode;

import org.testng.annotations.Test;

import java.util.Stack;

/**
 * @Description: 20. 有效的括号
 * 思路：按照从左至右的顺序将字符串压入栈中，一旦碰到闭括号则出栈与闭括号比较是否为1对
 * @Author: qufang
 * @Date: Created in 上午10:59 2018/12/1
 */
public class IsValid_20 {
    public boolean isValid(String s) {
        if (s.length() % 2 != 0)
            return false;
        else if (s.length() == 0)
            return true;
        else {
            Stack stack = new Stack();
            for (int i = 0; i < s.length(); i++) {
                if (ifLeft(s.charAt(i))) {
                    stack.add(s.charAt(i));
                }else if(ifRight(s.charAt(i)) & stack.size()>0){
                    char temp=(char)stack.pop();
                    if(!compareBrackets(temp,s.charAt(i))){
                        return false;
                    }
                }else if(ifRight(s.charAt(i)) & stack.size()==0){
                    return false;
                }
            }
            if (stack.size()==0)
                return true;
        }
        return false;
    }

    //判断是否为左括号，如果为左括号则压栈
    public boolean ifLeft(char a){
        if (a=='(' || a=='{' || a=='[')
            return true;
        return false;
    }
    //判断是否为右括号，如果为又括号，则出栈并与此右括号进行比较
    public boolean ifRight(char a){
        if (a==')' || a=='}' || a==']')
            return true;
        return false;
    }

    //判断括号是否匹配
    public boolean compareBrackets(char a,char b){
        if(a=='(' & b==')')
            return true;
        else if(a=='[' & b==']')
            return true;
        else if(a=='{' & b=='}')
            return true;
        else return false;
    }

    @Test
    public void test1(){
        System.out.println(isValid(""));
    }
    @Test
    public void test2(){
        System.out.println(isValid("[]]"));
    }
    @Test
    public void test3(){
        System.out.println(isValid("{[]}"));
    }

    @Test
    public void test4(){
        System.out.println(isValid("([)]"));
    }
    @Test
    public void test5(){
        System.out.println(isValid("))))"));
    }

    @Test
    public void test6(){
        System.out.println(isValid("(((("));
    }
    @Test
    public void test7(){
        System.out.println(isValid("(]"));
    }

}

